function createdummyemployees(request, response) {
	var roleID = 1;
	var lastI = request.getParameter('lasti') || '0';
	lastI = parseInt(lastI);
	var lastproc = request.getParameter('lastproc') || 'inactive';
	var thisContext = nlapiGetContext();
	var MAX_USAGE = 30;

	//Create Supervisors
	/*if (lastproc == "supervisors") {
		for (var i = lastI; i < 500; i++) {
			if (thisContext.getRemainingUsage() > MAX_USAGE) {
				var newemp = nlapiCreateRecord('employee');
				newemp.setFieldValue('firstname', 'Supervisor');
				newemp.setFieldValue('lastname', i + 'case2722');
				newemp.setFieldValue('email', 'supervisor2722' + i + '@netsuite.com');
				newemp.setFieldValue('giveaccess', 'T');
				newemp.setLineItemValue('roles', 'selectedrole', 1, roleID);
				newemp.setFieldValue('password', 'FLODocs2017!');
				newemp.setFieldValue('password2', 'FLODocs2017!'); 
				var empid = nlapiSubmitRecord(newemp, false, true);
				nlapiLogExecution('debug', 'supervisor', empid);
			} else {
				nlapiSetRedirectURL('SUITELET', thisContext.getScriptId(), 1, false, {
					lasti: i,
					lastproc: "supervisors"
				});
				return;
			}


		}
		lastI = 0;
		lastproc = "inactive"
	}*/

	if (lastproc == "inactive") {
		for (var i = lastI; i < 100; i++) {
			if (thisContext.getRemainingUsage() > MAX_USAGE) {

				/*var supervisorIndex = i % 100;
				var supervisorSearch = nlapiSearchRecord('employee', null, [
					['firstname', 'is', 'Supervisor'], 'AND', ['lastname', 'is', supervisorIndex + 'case2722']
				], [new nlobjSearchColumn('internalid')]);
				var supervisorId = supervisorSearch[0].getId();*/
				var newemp = nlapiCreateRecord('employee');
				newemp.setFieldValue('firstname', 'Inactivate');
				newemp.setFieldValue('lastname', i + 'NS-2103');
				newemp.setFieldValue('email', 'NS2103' + i + '@netsuite.com');
				//newemp.setFieldValue('supervisor', supervisorId);
				newemp.setFieldValue('giveaccess', 'T');
				newemp.setLineItemValue('roles', 'selectedrole', 1, roleID);
				newemp.setFieldValue('password', 'FLODocs2017!');
				newemp.setFieldValue('password2', 'FLODocs2017!'); /*newemp.setFieldValue('subsidiary','3');*/
				var empid = nlapiSubmitRecord(newemp, false, true);
				nlapiLogExecution('debug', 'empid', empid);
			} else {
				nlapiSetRedirectURL('SUITELET', thisContext.getScriptId(), 1, false, {
					lasti: i,
					lastproc: "inactive"
				});
				return;
			}


		}
		lastI = 100;
		lastproc = "active"
	}

	if (lastproc == "active") {
		for (var i = lastI; i < 100; i++) {
			if (thisContext.getRemainingUsage() > MAX_USAGE) {

				/*var supervisorIndex = i % 100;
				var supervisorSearch = nlapiSearchRecord('employee', null, [
					['firstname', 'is', 'Supervisor'], 'AND', ['lastname', 'is', supervisorIndex  + 'case2722']
				], [new nlobjSearchColumn('internalid')]);
				var supervisorId = supervisorSearch[0].getId();*/
				var newemp = nlapiCreateRecord('employee');
				newemp.setFieldValue('firstname', 'Active');
				newemp.setFieldValue('lastname', i + 'NS-2103');
				newemp.setFieldValue('email', 'NS2103' + i + '@netsuite.com');
				//newemp.setFieldValue('supervisor', supervisorId);
				newemp.setFieldValue('giveaccess', 'T');
				newemp.setLineItemValue('roles', 'selectedrole', 1, roleID);
				newemp.setFieldValue('password', 'FLODocs2017!');
				newemp.setFieldValue('password2', 'FLODocs2017!'); /*newemp.setFieldValue('subsidiary','3');*/
				var empid = nlapiSubmitRecord(newemp, false, true);
				nlapiLogExecution('debug', 'empid', empid);
			} else {
				nlapiSetRedirectURL('SUITELET', thisContext.getScriptId(), 1, false, {
					lasti: i,
					lastproc: "active"
				});
				return;
			}


		}
	}
	response.write('done!')
}